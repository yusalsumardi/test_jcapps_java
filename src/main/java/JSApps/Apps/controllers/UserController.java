package JSApps.Apps.controllers;


import JSApps.Apps.entities.UserRegisterEntity;
import JSApps.Apps.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping(value = "/api")
public class UserController {

    @Autowired
    UserService userService;

    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public UserController() throws IOException {
    }

    @PostMapping(value = "register")
    public ResponseEntity<?> addUser(@RequestBody UserRegisterEntity param) {
        if (userService.emailExist(param)) {
            try {
                param.setPassword(passwordEncoder().encode(param.getPassword()));
                userService.addUser(param);
                System.out.println("Register Success");
                return new ResponseEntity<>(param, HttpStatus.OK);
            } catch (Exception e) {
                System.out.println(e);
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
