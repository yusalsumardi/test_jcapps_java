package JSApps.Apps.service;

import JSApps.Apps.entities.UserRegisterEntity;
import org.springframework.stereotype.Component;



@Component
public interface UserService {
    void addUser(UserRegisterEntity param);
    public boolean emailExist(UserRegisterEntity user);
    UserRegisterEntity findUserByEmail(UserRegisterEntity param);
}
