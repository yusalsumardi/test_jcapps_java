package JSApps.Apps.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.Reader;

public class MyBatisUtil {

    @Autowired
    private static SqlSessionFactory sessionFactory;

    static {
        Reader reader;
        try {
            reader = Resources.getResourceAsReader("config/SqlMapConfig.xml");
            sessionFactory = new SqlSessionFactoryBuilder().build(reader);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static SqlSessionFactory getSessionFactory(){
        return sessionFactory;
    }
}
