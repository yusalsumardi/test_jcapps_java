package JSApps.Apps.entities;

import java.sql.Timestamp;
import java.util.Date;

public class UserRegisterEntity {
    private long unique_id;
    private String firstName;
    private String lastName;
    private String email;
    private int phone_number;
    private Date birth_date;
    private String sex;
    private String password;
    private Timestamp create_date;
    private Timestamp update_date;


    public long getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(long unique_id) {
        this.unique_id = unique_id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(int phoneNumber) {
        this.phone_number = phoneNumber;
    }

    public Date getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(Date birthdate) {
        this.birth_date = birthdate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String gender) {
        this.sex = gender;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Timestamp getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Timestamp created_date) {
        this.create_date = created_date;
    }

    public Timestamp getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Timestamp update_date) {
        this.update_date = update_date;
    }

    @Override
    public String toString() {
        return "UserRegisterEntity{" +
                "unique_id=" + unique_id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", phone_number=" + phone_number +
                ", birthdate='" + birth_date + '\'' +
                ", gender='" + sex + '\'' +
                ", password='" + password + '\'' +
                ", created_date=" + create_date +
                ", update_date=" + update_date +
                '}';
    }
}
