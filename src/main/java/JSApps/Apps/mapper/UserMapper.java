package JSApps.Apps.mapper;

import JSApps.Apps.entities.UserRegisterEntity;
import JSApps.Apps.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserMapper {

    public void addUser(UserRegisterEntity user) {
        SqlSession session = MyBatisUtil.getSessionFactory().openSession();
        session.insert("insertUser", user);
        session.commit();
        session.close();
    }

    public UserRegisterEntity findUserByEmail(UserRegisterEntity user){
        SqlSession session = MyBatisUtil.getSessionFactory().openSession();
        UserRegisterEntity users = session.selectOne("findUserByEmail", user);
        session.commit();
        session.close();
        return users;
    }
}
